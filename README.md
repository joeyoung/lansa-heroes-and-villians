# README #

LANSA rendition of Heroes and Villians

### What is this repository for? ###

* Demonstrates modern web development with Visual LANSA
* Uses LANSA 14 SP 2
* [LANSA](https://www.lansa.com/)

### How do I get set up? ###

* Create a new Partition called HAV
* Clone this repo into the Parition
